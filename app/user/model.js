const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


// Schema
let userSchema = new mongoose.Schema({
    idCustomer: { type: Number, default: -1 },
    email: {type: String, required: 'E-mail requis.', unique: true},
    password: {type: String, required: 'Mot de Passe requis.', minlength: [8, 'Mot de passe doit contenir au moins 8 charactères']},
    firstName: String,
    lastName: String,
    phone: String,
    dateCreated: Date,
    enterprise: String,
    address: {
        idAddress: { type: Number, default: -1 },
        active: {type: Boolean, default: true},
        invoice: {type: Boolean, default: false},
        delivery: {type: Boolean, default: false},
        street: String,
        building_name: String,
        city: String,
        country: String,
        zip: String,
    },
    active: {type: Boolean, default: false},
    isCommercial: {type: Boolean, default: false},
    passwordModified: {type: Boolean, default: true},
    saltSecret: {type: String, default: ''},
});


// Custom validation for email
userSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'E-Mail invalide.');


// Pre sauvegarde (Salage du mot de passe)
userSchema.pre('save', function (next) {
    if (this.passwordModified) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(this.password, salt, (err, hash) => {
                this.password = hash;
                this.saltSecret = salt;
                this.passwordModified = false;
                next();
            });
        });
    } else {
        next();
    }
});


// Verification du mot de passe avec décryptage
userSchema.methods.verifyPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};


// JWT generator
userSchema.methods.generateJwt = function () {
    return jwt.sign(
        {_id: this._id, isCommercial: this.isCommercial},
        process.env.JWT_SECRET,
        {expiresIn: process.env.JWT_EXP}
    );
};

// Register in mongoose
mongoose.model('User', userSchema);
