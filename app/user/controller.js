const mongoose = require('mongoose');
const passport = require('passport');
const axios = require('axios');
const User = mongoose.model('User');

module.exports.register = (req, res, next) => {
    let user = new User({
        email: req.body.email,
        password: req.body.password,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        phone: req.body.phone,
        dateCreated: req.body.dateCreated,
        enterprise: req.body.enterprise,
        isCommercial: req.body.isCommercial,
        active: req.body.active,
        address: {
            active: req.body.active,
            invoice: req.body.invoice,
            delivery: req.body.delivery,
            building_name: req.body.building_name,
            street: req.body.street,
            country: req.body.country,
            city: req.body.city,
            zip: req.body.zip,
        },
    });
    /*axios
        .post(process.env.ESB_URL + ESB_URL_POST_USER, {
            "client": {
                "nom": `${user.firstName}`,
                "prenom": `${user.lastName}`,
                "dette": 0,
                "restant_du": 0
            },
            "adresse": {
                "active": true,
                "facturation": true,
                "livraison": true,
                "nom_batiment": "",
                "rue": "",
                "code_postale": "",
                "ville": "",
                "pays": ""
            }
        })
        .then(ares => {
            user.idCustomer = ares.data.id
            user.address.idAddress = ares.data.id
            user.save((err, doc) => {
                if (!err) {
                    return res.json(doc);
                } else {
                    return res.status(400).json(err)
                }
            })
        }).catch(err => {
        console.log(err)
        return res.status(err.response.status).json(err)
    })*/
    user.save((err, doc) => {
        if (!err) {
            return res.json(doc);
        } else {
            return res.status(400).json(err)
        }
    });
}


module.exports.authenticate = (req, res, next) => {
    console.log("[login]")
    passport.authenticate('local', (err, user, info) => {
        if (err) {
            console.log("400 : " + JSON.stringify(err));
            return res.status(400).json(err)
        } else if (user) {
            if (user.active) {
                return res.status(200).json({
                    "token": user.generateJwt()
                });
            } else {
                return res.status(200).json({
                    "active": false
                })
            }
        } else {
            console.log("Nope 404: " + JSON.stringify(info));
            return res.status(404).json(info);
        }
    })(req, res);
}


module.exports.getUsers = (req, res, next) => {
    User.find({}, function (err, users) {
        res.json(users);
    })
}

module.exports.getCurrentUser = (req, res) => {
    User.find({}, function (err, users) {
        users.forEach(user => {
            if (req.params.id === user.id) {
                res.json(user)
            }
        })
    })
}

module.exports.getUser = (req, res) => {
    console.log(req)
    User.find({}, function (err, users) {
        users.forEach(user => {
            if (req.query.ID === user.id) {
                res.json(user)
            }
        })
    })
}

module.exports.editUser = (req, res) => {
    User.find({}, function (err, users) {
        users.forEach(user => {
            if (req.query.ID === user.id) {
                res.json(user)
            }
        })
    })
}

module.exports.deleteUser = (req, res) => {
    User.find({}, function (err, users) {
        users.forEach(user => {
            console.log(req.params.id)
            if (req.params.id === user.id) {
                user.remove((err, doc) => {
                    if (!err) {
                        return res.json(doc);
                    } else {
                        return res.status(400).json(err)
                    }
                });
            }
        })
    })
}
