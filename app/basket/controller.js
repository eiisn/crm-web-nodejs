const mongoose = require('mongoose');
const Basket = mongoose.model('Basket')


module.exports.getActiveUserBasket = (req, res, next) => {
    console.log("[Get user basket] : " + req._id)
    Basket.find({'userId': req._id, 'active': true}, function(err, basket) {
        if (basket.length) {
            if (basket.length > 1) {
                for (let i = 0; i < basket.length; i++) {
                    basket[i].active = false;
                    basket[i].save()
                }
                let newBasket = new Basket()
                newBasket.userId = req._id
                newBasket.save((err, doc) => {
                    if (!err) {
                        return res.json(doc)
                    } else {
                        return res.status(400).json(err)
                    }
                })
            } else {
                return res.json(basket[0])
            }
        } else {
            let newBasket = new Basket()
            newBasket.userId = req._id
            newBasket.save((err, doc) => {
                if (!err) {
                    res.json(doc)
                } else {
                    return res.status(400).json(err)
                }
            })
        }
    }).catch(err => {
        return res.json({"data": err})
    })
}


module.exports.getPreviousBasket = (req, res, next) => {
    Basket.find({'userId': req._id, 'active': false}, function (err, basket) {
        return res.json(basket)
    }).catch(err => {
        return res.json(err)
    })
}


module.exports.addProductToBasket = (req, res, next) => {
    console.log("[Add to basket] : " + req.body.basketId)
    Basket.findById(req.body.basketId, function(err, basket) {
        if (err) {
            return res.json(err)
        } else {
            let item = basket.items.find(i => i.id === req.body.product.id)
            if (item !== undefined) {
                item.quantity += 1
            } else {
                item = req.body.product
                item.quantity = 1
                basket.items.push(item)
            }

            basket.save((err, doc) => {
                if (err) {
                    return res.json(err)
                } else {
                    return res.json(doc)
                }
            })
        }
    }).catch(err => {
        return res.json(err)
    })
}


module.exports.removeProductFromBasket = (req, res, next) => {
    console.log("[Remove from basket] :" + req.body.basketId)
    Basket.findById(req.body.basketId, function(err, basket) {
        if (err) {
            return res.json(err)
        } else {
            let item = basket.items.find(i => i.id === req.body.product.id)
            if (item === undefined) {
                return res.status(400).json({"data": "Error no product " + JSON.stringify(req.body.product)})
            } else {
                if (item.quantity >= 2) {
                    item.quantity -= 1
                    basket.save((err, doc) => {
                        if (err) {
                            return res.json(err)
                        } else {
                            return res.json(doc)
                        }
                    })
                } else {
                    removeIndex = basket.items.map(item => { return item.id }).indexOf(req.body.product.id)
                    basket.items.splice(removeIndex, 1)
                    basket.save((err, doc) => {
                        if (err) {
                            return res.json(err)
                        } else {
                            return res.json(doc)
                        }
                    })
                }
            }
        }
    }) 
}
