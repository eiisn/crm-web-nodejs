const mongoose = require('mongoose');


let basketSchema = new mongoose.Schema({
    userId: mongoose.Schema.Types.ObjectId,
    active : {
        type: Boolean,
        default: true
    },
    items: [{
        id: Number,
        name: String,
        list_price: Number,
        quantity: Number
    }],
    total_price: Number,
    end_date: Date
})


basketSchema.pre('save', function(next) {
    let total = this.items.reduce((a,b) => { 
        return a.list_price == null ? a + b.list_price * b.quantity : b + a.list_price * a.quantity
    }, 0)
    this.total_price = total
    next()
})


mongoose.model('Basket', basketSchema);
