const mongoose = require('mongoose');


let shipmentSchema = new mongoose.Schema({
    userId: {type: String, required: true},
    basket: mongoose.Schema.Types.ObjectId,
    state: String,
    delivery_date: Date,
    reference: String,
    erpId: Number
})


mongoose.model('Shipment', shipmentSchema);
