const axios = require('axios');
const mongoose = require('mongoose');
const Shipment = mongoose.model('Shipment');
const Basket = mongoose.model('Basket')


module.exports.addShipment = (req, res, next) => {
    console.log("[Add Shipment]")
    axios
        .get(process.env.ESB_URL + ESB_URL_GET_STATUS_SHIPMENT, {
            "currency": req.body.currency,
            "information": req.body.information,
            "customer": req.body.customer,
            "detail": req.body.detail,
        })
        .then(ares => {
            let shipment = new Shipment({
                userId: req._id,
                basket: req.body.basketId,
                state: ares.data.state,
                delivery_date: ares.data.date,
                reference: ares.data.reference,
                erpId: ares.data.id
            })

            shipment.save((err, doc) => {
                if (!err) {
                    Basket.findById(req.body.basketId, (err, basket) => {
                        basket.active = false
                        basket.save()
                    })
                    return res.json(doc)
                } else {
                    return res.status(400).json(err)
                }
            })
            return res.send(ares.data)
        })
        .catch(err => {
            return res.send(err)
        })
}

