const axios = require('axios');
const mongoose = require('mongoose');

module.exports.getProducts = (req, res, next) => {
    axios
        .get(process.env.ESB_URL + process.env.ESB_URL_GET_PRODUCTS)
        .then(ares => {
            return res.json(ares.data)
        })
        .catch(err => {
            if (err.response) {
                return res.status(err.response.status).json(err)
            }
            console.log(JSON.stringify(err))
            return res.status(400).json(err)
        })
}
