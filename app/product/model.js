const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

let productSchema = new mongoose.Schema({
    productId: {type: String, required: "Product Id required", unique: true},
    list_price: {type: Number},
    type: {type: String},
    name: {type: String}
})

// Register in mongoose
mongoose.model('Product', productSchema)
