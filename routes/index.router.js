const express = require('express');
const router = express.Router();

const ctrlUser = require('../app/user/controller');
const ctrlProduct = require('../app/product/controller');
const ctrlShipment = require('../app/shipment/controller');
const ctrlBasket = require('../app/basket/controller')
const jwtHelper = require('../config/jwtHelper');

// User
router.post('/register', ctrlUser.register);
router.post('/login', ctrlUser.authenticate);
router.get('/users', ctrlUser.getUsers);
router.get('/get-user', ctrlUser.getUser);
router.get('/user/:id', ctrlUser.getCurrentUser);
router.post('/users/remove/:id', ctrlUser.deleteUser);

// PRODUCT
router.get('/products', jwtHelper.verifyJwtToken, ctrlProduct.getProducts);

// BASKET
router.get('/basket', jwtHelper.verifyJwtToken, ctrlBasket.getActiveUserBasket)
router.get('/basket/previous', jwtHelper.verifyJwtToken, ctrlBasket.getPreviousBasket)
router.post('/basket/add-item', jwtHelper.verifyJwtToken, ctrlBasket.addProductToBasket)
router.post('/basket/remove-item', jwtHelper.verifyJwtToken, ctrlBasket.removeProductFromBasket)

// SHIPMENT
router.post('/add-shipment', ctrlShipment.addShipment);

module.exports = router;
