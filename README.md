# Pour créer un Utilisateur Commercial :
Avec n'importe quel outil qui permet de faire des requête HTTP 
>(Cf. Postman)

__Faire une requête POST avec en body :__
```json
{
	firstName: "tartenpion",
	email: "commercial@test.com",
	lastName: "tartenfion",
	address: "nope",
	country: "nope",
	city: "nope",
	password: "adminadmin",
	isCommercial: true
}
```
